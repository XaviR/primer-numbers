// program to check if a number is prime or not

// take input from the user
const number = parseInt(prompt('Introduce un numero positivo: '))
let isPrime = true

// check if number is equal to 1
if (number === 1) {
    alert('1 es numero primo')
}

// check if number is greater than 1
else if (number > 1) {
    // looping through 2 to number-1
    for (let i = 2; i < number; i++) {
        if (number % i == 0) {
            isPrime = false
            break
        }
    }

    if (isPrime) {
        alert(`${number} es un numero primo`)
    } else {
        alert(`${number} no es un numero primo`)
    }
}

// check if number is less than 1
else {
    alert('El numero no es primo.')
}

